#version 430 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;

uniform vec3 cameraPos;
uniform mat4 model;
uniform mat4 mvp;

void main()
{
    gl_Position = mvp * vec4(position, 1.0f);

	//diffuse
	FragPos = vec3(model * vec4(position, 1.0));
	Normal = normal;
    TexCoords = texCoords;
}