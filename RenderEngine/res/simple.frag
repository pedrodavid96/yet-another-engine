#version 430 core

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec3 toLightVector;
in vec3 toCameraVector;

out vec4 color;

uniform vec3 lightPos;
uniform vec3 lightColor;

uniform mat4 view;

uniform sampler2D texture_diffuse1;

void main()
{
	//color = vec4(vColor, 1.0f);

	vec3 normal = normalize(Normal);
	vec3 unitLightVector = normalize(toLightVector);
	vec3 unitCameraVector = normalize(toCameraVector);

	vec3 toLightVector = lightPos - FragPos;
	vec3 toCameraVector = (inverse(view) * vec4(0.0, 0.0, 0.0, 1.0)).xyz - FragPos;

	vec3 lightDirection = -unitLightVector;
	vec3 reflectedLightDirection = reflect(lightDirection, Normal);
	
	float shineDamper = 10.0;
	float reflectivity = 1;
	float specFac = pow(max(dot(reflectedLightDirection, unitCameraVector), 0.0), shineDamper);
	vec3 spec = lightColor * reflectivity * specFac;

	vec3 lightDir = normalize(lightPos - FragPos);
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;
	color = vec4(diffuse, 1.0) * texture(texture_diffuse1, TexCoords) + vec4(spec, 1.0);
}