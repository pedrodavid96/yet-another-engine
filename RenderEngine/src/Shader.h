#pragma once

#include <glad/glad.h>
#include <string>
#include <memory>
#include "./FrameworkException.h"

namespace core::graphics
{
	class ShaderException : public FrameworkException {
	public:
		ShaderException(std::string message) : FrameworkException("Shader", message) { }
	};

	class Shader
	{
	public:
		enum class Type
		{
			Vertex = GL_VERTEX_SHADER,				//.vert
			Tess_ctr = GL_TESS_CONTROL_SHADER,		//.tesc
			Tess_ev = GL_TESS_EVALUATION_SHADER,	//.tese
			Geometry = GL_GEOMETRY_SHADER,			//.geom
			Fragment = GL_FRAGMENT_SHADER,			//.frag
			Compute = GL_COMPUTE_SHADER				//.comp
		};

		Shader::Shader(Type type, std::string source) :
			id{ glCreateShader(static_cast<GLenum>(type)) }
		{
			auto c_src = source.c_str();
			glShaderSource(id, 1, &c_src, NULL);
			glCompileShader(id);

			GLint status;
			glGetShaderiv(id, GL_COMPILE_STATUS, &status);
			if (status == GL_FALSE) {
				GLint length;
				glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
				auto log = std::make_unique<GLchar[]>(length);
				glGetShaderInfoLog(id, length, &length, log.get());
				throw ShaderException(log.get());
			}
		}
		Shader(const Shader& shader) = delete;
		Shader(Shader&& shader) = delete;
		inline ~Shader() { glDeleteShader(id); }

		const GLuint id;
	};

	class ShaderProgram
	{
	public:
		ShaderProgram(std::initializer_list<std::reference_wrapper<const Shader>> shaders) :
			id { glCreateProgram() }
		{
			for (auto shader : shaders) { glAttachShader(id, shader.get().id); }
			glLinkProgram(id);
			glValidateProgram(id);
		}
		inline ~ShaderProgram() { glDeleteProgram(id); }

		inline void Enable() { glUseProgram(id); }
		inline void Disable() { glUseProgram(0); }

		inline GLint GetUniformLocation(const GLchar *name) { return glGetUniformLocation(id, name); }
		inline void UniformMat4(GLint location, GLfloat* matrix) { glUniformMatrix4fv(location, 1, GL_FALSE, matrix); }

		const GLuint id;
	};
}
