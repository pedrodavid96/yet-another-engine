#pragma once

#include <string>

namespace core::graphics
{
	class Window
	{
	public:
		Window(std::string title, size_t width, size_t height);
		~Window();
		bool ShouldClose();
		void Update();
	//private: TODO:
		void *window;
	};
}