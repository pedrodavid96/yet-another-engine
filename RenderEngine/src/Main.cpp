#include "Window.h"

#include "Shader.h"
#include "VAO.h"
#include "VBO.h"
#include "EBO.h"
#include "Texture.h"

#include <iostream>

#include <istream>
#include <fstream>
#include <sstream>

#include <vector>
#include <chrono>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SOIL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

auto readFile(const char* source) {
	auto file = std::ifstream{ source };
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	std::stringstream stream;
	stream << file.rdbuf();
	return stream.str();
}

struct spatial { 
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
	spatial(glm::vec3 position = glm::vec3(1.0f), glm::vec3 rotation = glm::vec3(0.0f), glm::vec3 scale = glm::vec3(1.0f)) : position{ position }, rotation{ rotation }, scale{ scale } { }
};

//int main() {
//	using namespace core::graphics;
//
//	Window window{ "Hello Triangle", 1280, 720 };
//
//	Shader v{ Shader::Type::Vertex, readFile("res/simple.vert") };
//	Shader f{ Shader::Type::Fragment, readFile("res/simple.frag") };
//	ShaderProgram shader{ v, f };
//	shader.Enable();
//
//	VAO vao{};
//	vao.Enable();
//	
//	//VERTEX VBO
//	VBO vertices{ std::array<GLfloat, 24> {
//		-0.5f, -0.5f, 0.0f,
//		0.5f, -0.5f, 0.0f,
//		0.5f,  0.5f, 0.0f,
//		-0.5f, 0.5f, 0.0f,
//		-0.5f, -0.5f, -0.5f,
//		0.5f, -0.5f, -0.5f,
//		0.5f,  0.5f, -0.5f,
//		-0.5f, 0.5f, -0.5f
//	}};
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
//	glEnableVertexAttribArray(0);
//
//	//COLOR VBO
//	VBO colors{ std::array<GLfloat, 24> {
//		1.0f, 0.0f, 1.0f,
//		1.0f, 0.0f, 0.0f,
//		1.0f, 0.0f, 1.0f,
//		0.0f, 0.0f, 1.0f,
//		1.0f, 0.0f, 1.0f,
//		1.0f, 0.0f, 0.0f,
//		1.0f, 0.0f, 1.0f,
//		0.0f, 0.0f, 1.0f
//	} };
//	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
//	glEnableVertexAttribArray(1);
//
//	//UV VBO
//	VBO uvs{ std::array<GLfloat, 16> {
//		0.0f, 0.0f,
//		1.0f, 0.0f,
//		1.0f, 1.0f,
//		0.0f, 1.0f,
//		0.0f, 0.0f,
//		1.0f, 0.0f,
//		1.0f, 1.0f,
//		0.0f, 1.0f
//	} };
//	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
//	glEnableVertexAttribArray(2);
//
//	//IBO/EBO index/element buffer object
//	EBO elements{ std::array<GLushort, 36> {
//		0, 1, 2,
//		2, 3, 0,
//		// top
//		3, 2, 6,
//		6, 7, 3,
//		// back
//		7, 6, 5,
//		5, 4, 7,
//		// bottom
//		4, 5, 1,
//		1, 0, 4,
//		// left
//		4, 0, 3,
//		3, 7, 4,
//		// right
//		1, 5, 6,
//		6, 2, 1
//	} };
//
//	//Textures
//	int width, height;
//	auto image = SOIL_load_image("res/container.jpg", &width, &height, 0, SOIL_LOAD_RGB);
//	Texture texture{};
//	texture.Enable();
//
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
//	glGenerateMipmap(GL_TEXTURE_2D);
//	SOIL_free_image_data(image); //TODO: RAII
//
//	shader.Enable();
//	auto mvpLocation = shader.GetUniformLocation("mvp");
//	auto entities = std::array<spatial, 3> {
//		spatial{ glm::vec3{1.0f, 0.0f, 0.0f} },
//		spatial{ glm::vec3{ -1.0f, 0.0f, 0.0f } },
//		spatial{ glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3{ 0.0f }, glm::vec3{ 0.25f, 0.5f, 0.0f } }
//	};
//
//	glm::mat4 projection = glm::perspective(45.0f, 1280.0f / 720.0f, 0.1f, 100.0f);
//	glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f));
//	
//	glEnable(GL_DEPTH_TEST);
//
//	std::chrono::high_resolution_clock clock{};
//	auto startTime = clock.now();
//	while (!window.ShouldClose())
//	{
//		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//		
//		shader.Enable();
//		vao.Enable();
//		
//		for (auto& spatial : entities) {
//			
//			glm::mat4 model = glm::translate(glm::mat4(1.0f), spatial.position);
//			model = glm::rotate(model, spatial.rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
//			model = glm::rotate(model, spatial.rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
//			model = glm::rotate(model, spatial.rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
//			model = glm::scale(model, spatial.scale);
//
//			glm::mat4 mvp = projection * view * model;
//
//			shader.UniformMat4(mvpLocation, glm::value_ptr(mvp));
//			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (GLvoid*)0);
//		}
//		
//		entities[1].rotation.y += 0.001f;
//		vao.Disable();
//		shader.Disable();
//		
//		window.Update();
//		auto endTime = clock.now();
//		auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);
//		startTime = endTime;
//		std::cout << elapsed.count() << " mus" << std::endl;
//	}
//
//	return 0;
//}

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture {
	GLuint id;
	std::string type;
	aiString path;
};

class Mesh {
public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures) :
		vertices{ vertices }, indices{ indices }, textures{ textures } 
	{
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);

		glBindVertexArray(this->VAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex),
			&this->vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
			&this->indices[0], GL_STATIC_DRAW);

		// Vertex Positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			(GLvoid*)0);
		// Vertex Normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			(GLvoid*)offsetof(Vertex, Normal));
		// Vertex Texture Coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			(GLvoid*)offsetof(Vertex, TexCoords));

		glBindVertexArray(0);
	}
	void Draw(core::graphics::ShaderProgram &shader)
	{
		GLuint diffuseNr = 1;
		GLuint specularNr = 1;
		for (GLuint i = 0; i < this->textures.size(); i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);

			std::string name = this->textures[i].type;
			std::string number = (name == "texture_diffuse") ? std::to_string(diffuseNr++) : std::to_string(specularNr++);

			glUniform1i(glGetUniformLocation(shader.id, ("material." + name + number).c_str()), i);
			glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
		}
		glActiveTexture(GL_TEXTURE0);

		// Draw mesh
		glBindVertexArray(this->VAO);
		glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

private:
	GLuint VAO, VBO, EBO;
};

GLint TextureFromFile(const char* path, std::string directory);

class Model
{
public:
	/*  Functions   */
	// Constructor, expects a filepath to a 3D model.
	Model(GLchar* path)
	{
		this->loadModel(path);
	}

	// Draws the model, and thus all its meshes
	void Draw(core::graphics::ShaderProgram &shader)
	{
		for (GLuint i = 0; i < this->meshes.size(); i++)
			this->meshes[i].Draw(shader);
	}

private:
	/*  Model Data  */
	std::vector<Mesh> meshes;
	std::string directory;
	std::vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

										/*  Functions   */
										// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	void loadModel(std::string path)
	{
		// Read file via ASSIMP
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals);
		// Check for errors
		if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
		{
			std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
			return;
		}
		// Retrieve the directory path of the filepath
		this->directory = path.substr(0, path.find_last_of('/'));

		// Process ASSIMP's root node recursively
		this->processNode(scene->mRootNode, scene);
	}

	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene)
	{
		// Process each mesh located at the current node
		for (GLuint i = 0; i < node->mNumMeshes; i++)
		{
			// The node object only contains indices to index the actual objects in the scene. 
			// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			this->meshes.push_back(this->processMesh(mesh, scene));
		}
		// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
		for (GLuint i = 0; i < node->mNumChildren; i++)
		{
			this->processNode(node->mChildren[i], scene);
		}

	}

	Mesh processMesh(aiMesh* mesh, const aiScene* scene)
	{
		// Data to fill
		std::vector<Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<Texture> textures;

		// Walk through each of the mesh's vertices
		for (GLuint i = 0; i < mesh->mNumVertices; i++)
		{
			Vertex vertex;
			glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
							  // Positions
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;
			vertex.Position = vector;
			// Normals
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
			// Texture Coordinates
			if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.TexCoords = vec;
			}
			else
				vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			vertices.push_back(vertex);
		}
		// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
		for (GLuint i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			// Retrieve all indices of the face and store them in the indices vector
			for (GLuint j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}
		// Process materials
		if (mesh->mMaterialIndex >= 0)
		{
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
			// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
			// Same applies to other texture as the following list summarizes:
			// Diffuse: texture_diffuseN
			// Specular: texture_specularN
			// Normal: texture_normalN

			// 1. Diffuse maps
			std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
			// 2. Specular maps
			std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		}

		// Return a mesh object created from the extracted mesh data
		return Mesh(vertices, indices, textures);
	}

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
	{
		std::vector<Texture> textures;
		for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
		{
			aiString str;
			mat->GetTexture(type, i, &str);
			// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
			GLboolean skip = false;
			for (GLuint j = 0; j < textures_loaded.size(); j++)
			{
				if (std::strcmp(textures_loaded[j].path.C_Str(), str.C_Str()) == 0)
				{
					textures.push_back(textures_loaded[j]);
					skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
					break;
				}
			}
			if (!skip)
			{   // If texture hasn't been loaded already, load it
				Texture texture;
				texture.id = TextureFromFile(str.C_Str(), this->directory);
				texture.type = typeName;
				texture.path = str;
				textures.push_back(texture);
				this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
			}
		}
		return textures;
	}
};




GLint TextureFromFile(const char* path, std::string directory)
{
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
	return textureID;
}

#include <GLFW/glfw3.h>


int main()
{
	using namespace core::graphics;
	
	Window window{ "Hello Triangle", 1280, 720 };
	
	Shader v{ Shader::Type::Vertex, readFile("res/simple.vert") };
	Shader f{ Shader::Type::Fragment, readFile("res/simple.frag") };
	ShaderProgram shader{ v, f };

	Model model{ "res/nanosuit/nanosuit.obj" };

	auto modelLocation = shader.GetUniformLocation("model");
	auto mvpLocation = shader.GetUniformLocation("mvp");

	auto lightPosLocation = shader.GetUniformLocation("lightPos");
	auto lightColorLocation = shader.GetUniformLocation("lightColor");
	glm::vec3 lightPos{ -1.0f, 2.0f, -4.0f };

	shader.Enable();
	glUniform3f(lightColorLocation, 1.0f, 1.0f, 1.0f);
	glfwSetWindowUserPointer((GLFWwindow*)window.window, &lightPos);
	auto keyCallback = [](GLFWwindow* mWindow, int key, int scancode, int action, int mode)
	{
		auto& lightPos = *static_cast<glm::vec3*>(glfwGetWindowUserPointer(mWindow));
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(mWindow, GL_TRUE);
		if (key == GLFW_KEY_W && action == GLFW_PRESS) {
			lightPos.y += 1.0f;
		}
		if (key == GLFW_KEY_S && action == GLFW_PRESS) {
			lightPos.y -= 1.0f;
		}
		if (key == GLFW_KEY_A && action == GLFW_PRESS) {
			lightPos.x -= 1.0f;
		}
		if (key == GLFW_KEY_D && action == GLFW_PRESS) {
			lightPos.x += 1.0f;
		}
		if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
			lightPos.z -= 1.0f;
		}
		if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
			lightPos.z += 1.0f;
		}
	};
	glfwSetKeyCallback((GLFWwindow*)window.window, keyCallback);

	glm::mat4 projection = glm::perspective(45.0f, 1280.0f / 720.0f, 0.1f, 100.0f);
	glm::vec3 cameraPos{ 0.0f, 0.0f, -16.0f };
	glUniform3f(shader.id, cameraPos.x, cameraPos.y, cameraPos.z);
	glm::mat4 view = glm::translate(glm::mat4(1.0f), cameraPos);
	glUniformMatrix4fv(glGetUniformLocation(shader.id, "view"), 1, false, glm::value_ptr(view));

	std::chrono::high_resolution_clock clock{};
	auto startTime = clock.now();
	glm::vec3 rotation{ 0.0f };
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	while (!window.ShouldClose())
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::mat4 modelV = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -7.0f, 0.0f));
		modelV = glm::rotate(modelV, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		modelV = glm::rotate(modelV, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
		modelV = glm::rotate(modelV, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
		modelV = glm::scale(modelV, glm::vec3(1.0f));
		
		glm::mat4 mvp = projection * view * modelV;
		
		shader.Enable();
		glUniform3fv(lightPosLocation, 1, glm::value_ptr(lightPos));
		glUniformMatrix4fv(modelLocation, 1, false, glm::value_ptr(modelV));
		shader.UniformMat4(mvpLocation, glm::value_ptr(mvp));
		
		rotation.y += 0.001f;

		model.Draw(shader);
		window.Update();
		auto endTime = clock.now();
		auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime);
		startTime = endTime;
		//std::cout << elapsed.count() << " mus" << std::endl;
	}

	return 0;
}
