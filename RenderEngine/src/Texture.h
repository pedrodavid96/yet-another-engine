#pragma once

#include <glad/glad.h>

namespace
{
	GLuint createTexture()
	{
		GLuint id;
		glGenTextures(1, &id);
		return id;
	}
}

namespace core::graphics 
{
	class Texture
	{
	public:
		inline Texture() : id{ createTexture() } { }
		Texture(const Texture&) = delete;
		Texture(Texture&&) = delete;

		inline ~Texture() { glDeleteTextures(1, &id); }
		inline void Enable() { glBindTexture(GL_TEXTURE_2D, id); }
		inline void Disable() { glBindTexture(GL_TEXTURE_2D, 0); }

	private:
		const GLuint id;
	};
}