#pragma once

#include <glad/glad.h>
#include <array>

namespace {
	GLuint createVertexBuffer()
	{
		GLuint id;
		glGenBuffers(1, &id);
		return id;
	}
}

namespace core::graphics
{
	class VBO
	{
	public:
		template <typename T, size_t N>
		inline VBO(std::array<T, N>&& buffer) : 
			id{ createVertexBuffer() } 
		{ 
			Enable();
			glBufferData(GL_ARRAY_BUFFER, N * sizeof(T), buffer.data(), GL_STATIC_DRAW);
		}
		VBO(const VBO&) = delete;
		VBO(VBO&&) = delete;

		inline ~VBO() { glDeleteBuffers(1, &id); }
		inline void Enable() { glBindBuffer(GL_ARRAY_BUFFER, id); }
		inline void Disable() { glBindBuffer(GL_ARRAY_BUFFER, 0); }

	private:
		const GLuint id;
	};
}