#pragma once

#include <exception>
#include <string>

namespace core {
	class FrameworkException : std::exception 
	{
	public:
		const std::string tag;
		const std::string message;
		FrameworkException(std::string tag, std::string message) : tag{ tag }, message{ message } { } //TODO(PedDavid): substitute string by enum class
		friend std::ostream& operator<<(std::ostream& stream, const FrameworkException& e)
		{
			return stream << "[" << e.tag << "] " << e.message;
		}
	};
}