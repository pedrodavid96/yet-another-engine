#pragma once

#include <glad/glad.h>
#include <array>

namespace {
	GLuint createElementBuffer()
	{
		GLuint id;
		glGenBuffers(1, &id);
		return id;
	}
}

namespace core::graphics
{
	class EBO
	{
	public:
		template <typename T, size_t N>
		inline EBO(std::array<T, N>&& buffer) : id{ createElementBuffer() }
		{ 
			Enable();
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, N * sizeof(T), buffer.data(), GL_STATIC_DRAW);
		}
		EBO(const EBO&) = delete;
		EBO(EBO&&) = delete;

		inline ~EBO() { glDeleteBuffers(1, &id); }
		inline void Enable() { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id); }
		inline void Disable() { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); }

	private:
		const GLuint id;
	};
}