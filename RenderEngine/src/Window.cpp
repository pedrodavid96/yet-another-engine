#include "Window.h"

#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <iostream>

#ifdef GLAD_DEBUG
void pre_gl_call(const char* name, void *funcptc, int len_args, ...)
{
	printf("[GL] %s (%d arguments)\n", name, len_args);
}
#endif

namespace core::graphics
{
	Window::Window(std::string title, size_t width, size_t height)
	{
		if (!glfwInit())
			throw "Failed to init GLFW";

		glfwSetErrorCallback([](int error, const char* description) { std::cout << error << ": " << description << std::endl; });
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		/* Create a windowed mode window and its OpenGL context */
		window = glfwCreateWindow(width, height, "Hello World", NULL, NULL);
		if (!window)
		{
			glfwTerminate();
			throw "Failed to Initialize Window";
		}

		/* Make the window's context current */
		glfwMakeContextCurrent((GLFWwindow*) window);
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
			throw "Failed to Load GL context";
		}

#ifdef GLAD_DEBUG
		glad_set_pre_callback(pre_gl_call);
		glad_debug_glClear = glad_glClear;
		glad_debug_glUseProgram = glad_glUseProgram;
		glad_debug_glBindVertexArray = glad_glBindVertexArray;
		glad_debug_glDrawArrays = glad_glDrawArrays;
		glad_debug_glDrawElements = glad_glDrawElements;
		glad_debug_glUniformMatrix4fv = glad_glUniformMatrix4fv;
		glad_debug_glGetUniformLocation = glad_glGetUniformLocation;
		glad_debug_glActiveTexture = glad_glActiveTexture;
		glad_debug_glUniform1i = glad_glUniform1i;
		glad_debug_glBindTexture = glad_glBindTexture;
		glad_debug_glUniform3fv = glad_glUniform3fv;
#endif

		std::cout << glGetString(GL_VERSION) << std::endl;
	}

	Window::~Window()
	{
		glfwTerminate();
	}

	bool Window::ShouldClose() 
	{ 
		return glfwWindowShouldClose(static_cast<GLFWwindow*>(window));
	}

	void Window::Update() 
	{
		glfwSwapBuffers(static_cast<GLFWwindow*>(window));
		glfwPollEvents();
	}
}