#pragma once

#include <glad/glad.h>

namespace {
	inline GLuint createVertexArray() {
		GLuint id;
		glGenVertexArrays(1, &id); //TODO(PedDavid): GL4.5 -> glCreateVertexArrays
		return id;
	}
}

namespace core::graphics
{
	class VAO
	{
	public:
		inline VAO() : id{ createVertexArray() } { }
		VAO(const VAO&) = delete;
		VAO(VAO&&) = delete;

		inline ~VAO() { glDeleteVertexArrays(1, &id); }
		inline void Enable() { glBindVertexArray(id); }
		inline void Disable() { glBindVertexArray(0); }

	private:
		const GLuint id;
	};
}