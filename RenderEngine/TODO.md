- Use static_cast

- Async IO (async asset loading for "infinite maps")
- Resource Manager (create new resource manager for folder x...)
- Shader taking "Initializer List" of RValues

- Exception Handling
- Exceptions Constructors Universal References?

- Shader, enum class values in source file to avoid glad leaking.
- Alternative to anonymous namespace since it didn't work (same fucntion name in different headers conflict)

- Assimp static linking, dealing with dynamic libraries

LOWER PRIORITY:

- No dependencies(despite glad)? Ordered by priority
	- SOIL
	- ASSIMP
	- GLM
	- GLFW